extends KinematicBody

# Declare member variables here. Examples:
# var a = 2
# var b = "text"



var rot = Vector2()
var dir = Vector3()
var vel = Vector3()

const MAX_SLOPE_ANGLE = 40

const GRAVITY = -40
const MAX_SPEED = 10
const JUMP_SPEED = 15
const ACCEL = 4.5
const DEACCEL= 6

const LOOKAROUND_SPEED = 0.01

# Called when the node enters the scene tree for the first time.
func _ready():
	set_process_input(true)
	
func _physics_process(delta):
	process_input(delta)
	process_movement(delta)
	
func is_mouse_mode_captured():
	return Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED
	
func _input(event):
	rot = Vector2()
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT and event.pressed and Input.get_mouse_mode() == Input.MOUSE_MODE_VISIBLE:
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
		
	if event is InputEventMouseMotion and is_mouse_mode_captured():
		rot.x += event.relative.x * LOOKAROUND_SPEED
		rot.y += event.relative.y * LOOKAROUND_SPEED
func process_input(delta):
	
	if Input.is_action_just_pressed("ui_cancel"):
		if Input.get_mouse_mode() == Input.MOUSE_MODE_VISIBLE:
			Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
		else:
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

	var input_movement_vector = Vector2()
	if Input.is_action_pressed("move_left"):
		input_movement_vector.x -= 1
	if Input.is_action_pressed("move_right"):
		input_movement_vector.x += 1
	if Input.is_action_pressed("move_front"):
		input_movement_vector.y -= 1
	if Input.is_action_pressed("move_back"):
		input_movement_vector.y += 1
		
	dir = Vector3()
	input_movement_vector = input_movement_vector.normalized()
	dir += transform.basis.z * input_movement_vector.y
	dir += transform.basis.x * input_movement_vector.x

	if is_on_floor():
	    if Input.is_action_just_pressed("ui_select"):
	        vel.y = JUMP_SPEED


func process_movement(delta):
	if not is_mouse_mode_captured():
		return
	if rot.x != 0: 
		rotate_object_local(Vector3(0, -1, 0), rot.x)
	if rot.y != 0:
		$camara.rotate_object_local(Vector3(-1, 0, 0), rot.y)
	rot = Vector2()
	
	dir.y = 0
	dir = dir.normalized()
	
	vel.y += delta * GRAVITY
	
	var hvel = vel
	hvel.y = 0
	
	var target = dir
	target *= MAX_SPEED
	
	var accel
	if dir.dot(hvel) > 0:
	    accel = ACCEL
	else:
	    accel = DEACCEL
	
	hvel = hvel.linear_interpolate(target, accel * delta)
	vel.x = hvel.x
	vel.z = hvel.z
	vel = move_and_slide(vel, Vector3(0, 1, 0), 0.05, 4, deg2rad(MAX_SLOPE_ANGLE))
