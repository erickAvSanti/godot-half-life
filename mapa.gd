tool
extends Spatial

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	for mat_index in $meshmap.get_mesh().get_surface_count():
		var mat = $meshmap.get_mesh().surface_get_material(mat_index)
		mat.set_cull_mode(SpatialMaterial.CULL_DISABLED)
		print(mat.get_class())

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
